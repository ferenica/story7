$(document).ready(function() {
    $(".set > a").on("click", function() {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this)
            .siblings(".content")
            .slideUp(200);
            $(".set > a i")
            .removeClass("fa-minus")
            .addClass("fa-plus");
        } else {
            $(".set > a i")
            .removeClass("fa-minus")
            .addClass("fa-plus");
            $(this)
            .find("i")
            .removeClass("fa-plus")
            .addClass("fa-minus");
            $(".set > a").removeClass("active");
            $(this).addClass("active");
            $(".content").slideUp(200);
            $(this)
            .siblings(".content")
            .slideDown(200);
        }
    });

    var clicked = false;
    $('#theme').click(function(){
        if(clicked){
            $('.navbar').css('background-color', 'white');
            $('.navbar-brand').css('color', 'black');
            $('body').css('background-color', '#556677');
            $('.set').css('background-color', '#f5f5f5');
            $('.set > a').css('color', '#555555');

            $('.content').css('background-color', 'white');
            $('.accordion-container > h2').css('color', 'white');
            $('button').css('background-color', '#556677');
            $('button').css('color', 'white');
            clicked = false;
        }else{
            $('.navbar').css('background-color', '#556677');
            $('.navbar-brand').css('color', 'white');
            $('body').css('background-color', 'white');
            $('.set').css('background-color', '#556677');
            $('.set > a').css('color', 'white');

            $('.content').css('background-color', '#f5f5f5');
            $('.accordion-container > h2').css('color', 'black');
            $('button').css('background-color', 'white');
            $('button').css('color', 'black');
            clicked = true;
        };

        return false;

    });

});
  