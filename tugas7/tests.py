from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *

#from selenium import webdriver
#from selenium.webdriver.common.keys import Keys
#from selenium.webdriver.chrome.options import Options
#import time
#from django.contrib.staticfiles.testing import StaticLiveServerTestCase

# Create your tests here.
class Story6_Web_Test(TestCase):
    
    def test_story7_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_using_landing_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'change.html')

    def test_story7_using_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)
    
    def test_words(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn('hai :D', html_response)

class Story6_Content_Test(TestCase):
    def test_aktivitas(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Aktivitas', html_response)
    
    def test_pendidikan(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Pendidikan', html_response)
    
    def test_pengalaman(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Pengalaman', html_response)

    def test_prestasi(self):
        request = HttpRequest()
        response = home(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Prestasi', html_response)

#class Story7_App_Functional_Test(StaticLiveServerTestCase):
#    def setUp(self):
#        chrome_options = Options()
#        chrome_options.headless = True

#        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
#        super(Story7_App_Functional_Test, self).setUp()

#    def tearDown(self):
#        self.selenium.quit()
#        super(Story7_App_Functional_Test, self).tearDown()

#    def test_accordion(self):
#        selenium = self.selenium
#        selenium.get(self.live_server_url)
#        time.sleep(5)

#        accordion = selenium.find_element_by_class_name('set')
#        content = selenium.find_element_by_class_name('content')

#        tampilan = content.value_of_css_property('display')
#        self.assertEqual(tampilan, 'none')

#        accordion.click()

#        tampilan = content.value_of_css_property('display')
#        self.assertEqual(tampilan, 'block')
